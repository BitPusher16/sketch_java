//import MurmurHash.java;

//import java.io.UnsupportedEncodingException;
//import java.io.File;
//import java.io.BufferedReader;
//import java.io.FileReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet; // used to compute unique outgoing edges for a vert;
import java.util.HashMap;
import java.util.Collections;
import java.util.Map;

class Sketch{
	int d;
	int w;
	int n; // edge count
    int[] hashSeeds;
    int[][] sketch;

	Sketch(int arg_d, int arg_w){
		d = arg_d;
		w = arg_w;
		n = 0;
        // create a vector of seeds for the hash function;
        // element i of this vector will be used to 
        // hash into row i of a sketch;
        hashSeeds = new int[d];
        int i;
        for(i = 0; i < d; i++){
            hashSeeds[i] = i + 20;
        }
        // allocate space for the sketch;
        sketch = new int[d][w];
	}

	void stats(){
		System.out.println("d:" + d + " w:" + w + " n:" + n);
	}

    long intToUnsignedLong(int x){
        return x & 0x00000000ffffffffL;
    }

    void addEdge(String argSrc, String argDst){
        String edgeString = argSrc + argDst;
        byte[] b = {};
        try{
            b = edgeString.getBytes("UTF-16");
        }
		catch(UnsupportedEncodingException e){
			System.out.println("unsupported encoding");
		}
        int i;
        for(i = 0; i < d; i++){ // what is scope of i if I declare it here?
            int intHash = MurmurHash.hash32(b, b.length, hashSeeds[i]);
            int colIdx = (int)(intToUnsignedLong(intHash) % w);
            sketch[i][colIdx]++;
        }
        n++;
    }

    int queryEdge(String argSrc, String argDst){
        String edgeString = argSrc + argDst;
        byte[] b = {};
        try{
            b = edgeString.getBytes("UTF-16");
        }
		catch(UnsupportedEncodingException e){
			System.out.println("unsupported encoding");
		}

        int min = Integer.MAX_VALUE;
        int i;
        for(i = 0; i < d; i++){
            int intHash = MurmurHash.hash32(b, b.length, hashSeeds[i]);
            int colIdx = (int)(intToUnsignedLong(intHash) % w);
            if(sketch[i][colIdx] < min){
                min = sketch[i][colIdx];
            }
        }
        return min;
    }
}

class Vert implements Comparable<Vert>{
    String id;
    List<String> destVerts;
    int outFrequency;   // total number of outgoing edges;
    int outDegree;      // total number of unique edge destinations;
    double fByD;        // outgoing edges divided by out degree;

    // constructor;
    Vert(String arg_id){
        id = arg_id;
        destVerts = new ArrayList<String>();
    }

    void AddDest(String dest){
        destVerts.add(dest);
    }

    void ComputeFByD(){
        // create HashSet;
        // could have used HashMap with conditional add() instead;
        HashSet<String> noDups = new HashSet<String>();
        // iterate over dest verts;
        int i;
        for(i = 0; i < destVerts.size(); i++){
            noDups.add(destVerts.get(i));
        }
        outFrequency = destVerts.size();
        outDegree = noDups.size();
        fByD = (double)outFrequency / (double)outDegree;
    }

    void PrintFByD(){
        System.out.println(id + " " +outFrequency+"/"+outDegree+"="+ fByD);
    }

    // implement compareTo so we can sort on fByD;
    // sorts descending;
    public int compareTo(Vert v){
        if (fByD > v.fByD) return -1;
        if (fByD < v.fByD) return 1;
        return 0;
    }
}

public class gSketch{
	public static void main(String[] args){

        // usage
        if(args.length != 6){
            System.out.println("usage: java gSketch [numRows] [numCols] "+
                "[numSketches] [sampleFile] [streamFile] [queryFile]"
            );
            System.exit(0);
        }

        // parse command line arguments;
		int d = Integer.parseInt(args[0]); // sketch rows;
		int w = Integer.parseInt(args[1]); // sketch cols (total);
		int r = Integer.parseInt(args[2]); // sketch partitions;
        File sample = new File(args[3]);   // sample file;
        File stream = new File(args[4]);   // stream file;
        File query = new File(args[5]);    // query file;
		System.out.println("num args:"+args.length+" "+d+" "+w+" "+r+
            " "+args[3]+" "+args[4]+" "+args[5]);

        // allocate memory for sketches;
        // if r does not divide w evenly,
        // total columns will be less than w;
		Sketch[] sArray = new Sketch[r];
		int i;
		for(i = 0; i < sArray.length; i++){
			sArray[i] = new Sketch(d, w/r);
		}
		for(i = 0; i < sArray.length; i++){
			//sArray[i].stats();
		}

        // read sample file;
        // order verts by freq_outgoing_edges/outdegree;
        // build hash table that maps source vert to one of r sketches;
        List<String> lines = new ArrayList<String>();
        try(BufferedReader br = new BufferedReader(new FileReader(sample))){
            String line;
            while((line = br.readLine()) != null){ // strips newline?
                //System.out.println(line);
                lines.add(line);
            }
            br.close();
        }
        catch(FileNotFoundException efnf){
            System.out.println("there was a problem");
        }
        catch(IOException eio){
            System.out.println("there was a problem");
        }

        String[] linesArr = lines.toArray(new String[lines.size()]);
        // use an ArrayList to store the verts;
        // use a HashMap to find them quickly;
        ArrayList<Vert> vertList = new ArrayList<Vert>();
        int idxNextVert = 0;
        HashMap<String, Integer> vertMap = new HashMap<String, Integer>();
        for(i = 0; i < linesArr.length; i++){
            //System.out.println(linesArr[i]);
            String src = linesArr[i].split("\t")[0];
            String dst = linesArr[i].split("\t")[1];
            //System.out.println(src + "--" + dst);
            if(vertMap.containsKey(src)){
                // existing vert;
                // use HashMap to find vert's location in ArrayList;
                int idx = vertMap.get(src);
                vertList.get(idx).AddDest(dst);
            }
            else{
                // new vert;
                // append the vert to ArrayList and store index in HashMap;
                vertList.add(idxNextVert, new Vert(src));
                vertMap.put(src, idxNextVert);
                // add current edge to new vert;
                vertList.get(idxNextVert).AddDest(dst);
                // update index to next location in ArrayList;
                idxNextVert++;

            }
        }

        // vert list has been built;
        // compute f/d for each vert and sort by f/d;
        for(i = 0; i < vertList.size(); i++){
            vertList.get(i).ComputeFByD();
            //vertList.get(i).PrintFByD();
        }
        //System.out.println(" ");
        Collections.sort(vertList);
        for(i = 0; i < vertList.size(); i++){
            //vertList.get(i).PrintFByD();
        }

        // verts are in order by f/d;
        // assign numSourceVerts/(r-1) verts to each sketch;
        // (leaves 1 sketch empty);
        HashMap<String, Integer> srcVertToSketchMap = 
            new HashMap<String, Integer>();
        int vertsPerSketch = (vertList.size()/(r-1)) + 1; // + 1 to round up;
        for(i = 0; i < vertList.size(); i++){
            srcVertToSketchMap.put(vertList.get(i).id, i/vertsPerSketch);
        }
        for(Map.Entry<String, Integer> entry : srcVertToSketchMap.entrySet()){
            //System.out.println(entry.getKey() + " " + entry.getValue());
        }

        // populate sketches;
        try(BufferedReader br = new BufferedReader(new FileReader(stream))){
            String line;
            while((line = br.readLine()) != null){ // strips newline?
                //System.out.println(line);
                String src = line.split("\t")[0];
                String dst = line.split("\t")[1];
                // use edge source vert to determine which sketch to store in;
                if(srcVertToSketchMap.containsKey(src)){
                    // we found a sketch for current edge;
                    int sketchIdx = srcVertToSketchMap.get(src);
                    sArray[sketchIdx].addEdge(src, dst);
                }
                else{
                    // edge goes in the outlier sketch;
                    int sketchIdx = sArray.length - 1;
                    sArray[sketchIdx].addEdge(src, dst);
                }
            }
            br.close();
        }
        catch(FileNotFoundException efnf){
            System.out.println("there was a problem");
        }
        catch(IOException eio){
            System.out.println("there was a problem");
        }

        // check that distribution of edges in sketches looks correct;
        for(i = 0; i < sArray.length; i++){
            //sArray[i].stats();
        }

        // sketches are populated;
        // open query file and compute runtime, query accuracy;
        try(BufferedReader br = new BufferedReader(new FileReader(query))){
            String line;
            double totalRelErr = 0;
            int numQueries = 0;
            while((line = br.readLine()) != null){ // strips newline?
                //System.out.println(line);
                String src = line.split("\t")[0];
                String dst = line.split("\t")[1];
                int ans = Integer.parseInt(line.split("\t")[2]);
                // use edge source vert to determine which sketch to query;
                if(srcVertToSketchMap.containsKey(src)){
                    int sketchIdx = srcVertToSketchMap.get(src);
                    int edgeEstimate = sArray[sketchIdx].queryEdge(src, dst);
                    totalRelErr += (double)(edgeEstimate - ans)/ans;
                    System.out.println(src+"\t"+dst+"\tsketch:"+sketchIdx+"\test:"+edgeEstimate+"\tans:"+ans);
                }
                else{
                    // edge is stored in the outlier sketch;
                    int sketchIdx = sArray.length - 1;
                    int edgeEstimate = sArray[sketchIdx].queryEdge(src, dst);
                    totalRelErr += (edgeEstimate - ans)/ans;
                    System.out.println(src+"\t"+dst+"\tsketch:"+sketchIdx+"\test:"+edgeEstimate+"\tans:"+ans);
                }
                numQueries++;
            }
            br.close();
            System.out.println("numQueries:" + numQueries);
            System.out.println("totalRelErr:" + totalRelErr);
            System.out.println("avgRelErr:" + totalRelErr/(double)numQueries);
        }
        catch(FileNotFoundException efnf){
            System.out.println("there was a problem");
        }
        catch(IOException eio){
            System.out.println("there was a problem");
        }

	} // end main();
}	



