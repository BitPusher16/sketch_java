//import MurmurHash.java;

//import java.io.UnsupportedEncodingException;
//import java.io.File;
//import java.io.BufferedReader;
//import java.io.FileReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet; // used to compute unique outgoing edges for a vert;
import java.util.HashMap;
import java.util.Collections;
import java.util.Map;
import java.util.Random;

class Sketch{
	int d;
	int w;
	int n; // edge count
    int[] hashSeeds;
    int rankHashSeed;
    int[][] sketch;
    int[][] rankVectors;
    int[][] currRank;   // for each cell of the sketch,
                        // stores the rank of the last edge 
                        // to write in that cell;

	Sketch(int arg_d, int arg_w){
		d = arg_d;
		w = arg_w;
		n = 0;
        rankHashSeed = 4321;
        int i, j;
        // create a vector of seeds for the hash function;
        // element i of this vector will be used to 
        // hash into row i of a sketch;
        hashSeeds = new int[d];
        for(i = 0; i < d; i++){
            hashSeeds[i] = i + 20;
        }
        // allocate space for the sketch;
        sketch = new int[d][w];

        // allocate space for rank vector matrix and populate such that
        // column vectors increase from 0 to d;
        rankVectors = new int[d][w];
        for(i = 0; i < d; i++){
            for(j = 0; j < w; j++){
                rankVectors[i][j] = i;
            }
        }
        // display the rank vectors;
        //for(i = 0; i < d; i++){
        //    for(j = 0; j < w; j++){
        //        System.out.print(rankVectors[i][j] + "\t");
        //    }
        //    System.out.print("\n");
        //}


        // make a temp copy of each rank matrix column vector,
        // shuffle the copy, then write the shuffled copy over the vector;
        int[] temp = new int[d];
        for(j = 0; j < w; j++){
            for(i = 0; i < d; i++){
                temp[i] = rankVectors[i][j];
            }
            FisherYatesShuffle(temp, j+20); // j + 20 is seed;
            for(i = 0; i < d; i++){
                rankVectors[i][j] = temp[i];
            }
        }
        // display the rank vectors;
        //for(i = 0; i < d; i++){
        //    for(j = 0; j < w; j++){
        //        System.out.print(rankVectors[i][j] + "\t");
        //    }
        //    System.out.print("\n");
        //}

        // allocate space for current rank matrix
        // and initialize all elements to -1;
        currRank = new int[d][w];
        for(i = 0; i < d; i++){
            for(j = 0; j < w; j++){
                currRank[i][j] = -1;
            }
        }
	}

    void FisherYatesShuffle(int[] array, long seed){
        int index;
        Random random = new Random(seed);
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            if (index != i) {
                array[index] ^= array[i];
                array[i] ^= array[index];
                array[index] ^= array[i];
            }
        }
    }

	void stats(){
		System.out.println("d:" + d + " w:" + w + " n:" + n);
	}

    // convert an integer to an unsigned long;
    // (technically, the long is still signed, but it
    // is guaranteed to be positive;)
    long intToUnsignedLong(int x){
        return x & 0x00000000ffffffffL;
    }

    void addEdge(String argSrc, String argDst){
        String edgeString = argSrc + argDst;
        byte[] b = {};
        try{
            b = edgeString.getBytes("UTF-16");
        }
		catch(UnsupportedEncodingException e){
			System.out.println("unsupported encoding");
		}
        // determine which rank vector this edge should use;
        // note: rankIdx identifies a column in rankVectors, not a row;
        int intRankHash = MurmurHash.hash32(b, b.length, rankHashSeed);
        int rankIdx = (int)(intToUnsignedLong(intRankHash) % w);

        // for each row in the sketch, hash into appropriate column
        // and update if current rank is >= rank of previous writer;
        int i;
        for(i = 0; i < d; i++){ // what is scope of i if I declare it here?
            int intHash = MurmurHash.hash32(b, b.length, hashSeeds[i]);
            int colIdx = (int)(intToUnsignedLong(intHash) % w);
            if(rankVectors[i][rankIdx] >= currRank[i][colIdx]){
                sketch[i][colIdx]++;
                currRank[i][colIdx] = rankVectors[i][rankIdx];
            }
        }
        n++;
    }

    int queryEdge(String argSrc, String argDst){
        String edgeString = argSrc + argDst;
        byte[] b = {};
        try{
            b = edgeString.getBytes("UTF-16");
        }
		catch(UnsupportedEncodingException e){
			System.out.println("unsupported encoding");
		}

        // determine which rank vector this edge should use;
        // note: rankIdx identifies a column in rankVectors, not a row;
        int intRankHash = MurmurHash.hash32(b, b.length, rankHashSeed);
        int rankIdx = (int)(intToUnsignedLong(intRankHash) % w);

        int min = Integer.MAX_VALUE;
        int i;
        for(i = 0; i < d; i++){
            int intHash = MurmurHash.hash32(b, b.length, hashSeeds[i]);
            int colIdx = (int)(intToUnsignedLong(intHash) % w);
            if(rankVectors[i][rankIdx] >= currRank[i][colIdx] && 
                sketch[i][colIdx] < min
            ){
                min = sketch[i][colIdx];
            }
        }
        return min;
    }
}

public class rSketch{
	public static void main(String[] args){

        // usage
        if(args.length != 4){
            System.out.println("usage: java rSketch [numRows] [numCols] "+
                "[streamFile] [queryFile]"
            );
            System.exit(0);
        }

        // parse command line arguments;
		int d = Integer.parseInt(args[0]); // sketch rows;
		int w = Integer.parseInt(args[1]); // sketch cols;
        File stream = new File(args[2]);   // stream file;
        File query = new File(args[3]);    // query file;
		System.out.println("num args:"+args.length+" "+d+" "+w+
            " "+args[2]+" "+args[3]);

        // allocate memory for sketch;
		Sketch s = new Sketch(d, w);

        // populate sketch;
        try(BufferedReader br = new BufferedReader(new FileReader(stream))){
            String line;
            while((line = br.readLine()) != null){ // strips newline?
                //System.out.println(line);
                String src = line.split("\t")[0];
                String dst = line.split("\t")[1];
                s.addEdge(src, dst);
            }
            br.close();
        }
        catch(FileNotFoundException efnf){
            System.out.println("there was a problem");
        }
        catch(IOException eio){
            System.out.println("there was a problem");
        }

        // sketch is populated;
        // open query file and compute runtime, query accuracy;
        try(BufferedReader br = new BufferedReader(new FileReader(query))){
            String line;
            double totalRelErr = 0;
            int numQueries = 0;
            while((line = br.readLine()) != null){ // strips newline?
                //System.out.println(line);
                String src = line.split("\t")[0];
                String dst = line.split("\t")[1];
                int ans = Integer.parseInt(line.split("\t")[2]);
                int edgeEstimate = s.queryEdge(src, dst);
                totalRelErr += (double)(edgeEstimate - ans)/ans;
                System.out.println(src+"\t"+dst+"\test:"+edgeEstimate+"\tans:"+ans);
                numQueries++;
            }
            br.close();
            System.out.println("numQueries:" + numQueries);
            System.out.println("totalRelErr:" + totalRelErr);
            System.out.println("avgRelErr:" + totalRelErr/(double)numQueries);
        }
        catch(FileNotFoundException efnf){
            System.out.println("there was a problem");
        }
        catch(IOException eio){
            System.out.println("there was a problem");
        }


	} // end main();
}	



