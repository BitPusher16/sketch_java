import java.util.HashMap;
import java.util.Map;
import java.io.*;

public class BuildQueries{
    public static void main(String[] args){

        if(args.length != 3){
            System.out.print("usage: java BuildQueries [randomizedStream] "+
                "[queryFile] [numQueries]\n"+
                "(randomizedStream must have different order "+
                "than streamFile)\n"
            );
            System.exit(0);
        }

        // parse command line arguments;
        File randStream = new File(args[0]);    // randomized stream;
        File queryFile = new File(args[1]);     // output file
        int numQ = Integer.parseInt(args[2]);
        
        try(BufferedReader br = new BufferedReader(new FileReader(randStream))){
            String line;
            int totalQ = 0;
            HashMap<String, Integer> qLines = new HashMap<String, Integer>();
            while((line = br.readLine()) != null){ // strips newline?
                //System.out.println(line);
                String src = line.split("\t")[0];
                String dst = line.split("\t")[1];
                if(totalQ < numQ){
                    // we still need more query entries;
                    if(qLines.containsKey(line)){
                        // we already have this edge in the hashmap;
                        // update edge count;
                        qLines.put(line, qLines.get(line) + 1);
                    }
                    else{
                        qLines.put(line, 1);
                        totalQ++;
                    }
                }
                else{
                    if(qLines.containsKey(line)){
                        qLines.put(line,qLines.get(line) + 1);
                    }
                }
            }
            br.close();

            // write out queries;
            try(BufferedWriter bw = new BufferedWriter(new 
                FileWriter(queryFile))
            ){
                for(Map.Entry<String, Integer> entry : 
                    qLines.entrySet()
                ){
                    bw.write(entry.getKey() + "\t" + entry.getValue() + "\n");
                }
            }

        }
        catch(FileNotFoundException efnf){
            System.out.println("there was a problem");
        }
        catch(IOException eio){
            System.out.println("there was a problem");
        }

    } // end main()
}
